### Implementing Distributed tracing & Log Aggregation inside microservices network using Spring Cloud Sleuth, Zipkin - PART 1
---

**Description:** This repository has six maven projects with the names **accounts, loans, cards, configserver, eurekaserver, gatewayserver** which are continuation from the 
section10 repository. All these microservices will be updated in this section to implement distributed tracing & log aggregation inside microservices network using **Spring Cloud 
Sleuth, Zipkin**. Below are the key steps that are followed inside this **section11** where we focused on set up of **Distributed tracing & Log Aggregation** 
inside our microservices network.

**Key steps:**
- Open the **pom.xml** of all the microservices **accounts, loans, cards, configserver, eurekaserver, gatewayserver** and make sure to add the below required dependency of 
  **Spring Cloud Sleuth** in all of them. 
```xml
<dependency>
  <groupId>org.springframework.cloud</groupId>
  <artifactId>spring-cloud-starter-sleuth</artifactId>
</dependency>
```
- Open the **AccountsController.java, LoansController.java , CardsController.java** and add the logger statements like we discussed in the course. This logger statements
  will help us to understand and validate how **Spring Cloud Sleuth** is going to add **App name, Trace ID, Span ID** information to the loggers inside the microservices.
  After making the changes your **AccountsController.java, LoansController.java , CardsController.java** should look like shown below,
  
### \accounts\src\main\java\com\openup\accounts\controller\AccountsController.java
  
```java
  /**
 * 
 */
package com.openup.accounts.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.openup.accounts.config.AccountsServiceConfig;
import com.openup.accounts.model.Accounts;
import com.openup.accounts.model.Cards;
import com.openup.accounts.model.Customer;
import com.openup.accounts.model.CustomerDetails;
import com.openup.accounts.model.Loans;
import com.openup.accounts.model.Properties;
import com.openup.accounts.repository.AccountsRepository;
import com.openup.accounts.service.client.CardsFeignClient;
import com.openup.accounts.service.client.LoansFeignClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;

/**
 * 
 *
 */

@RestController
public class AccountsController {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountsController.class);
	
	@Autowired
	private AccountsRepository accountsRepository;

	@Autowired
	AccountsServiceConfig accountsConfig;
	
	@Autowired
	LoansFeignClient loansFeignClient;

	@Autowired
	CardsFeignClient cardsFeignClient;
	
	@PostMapping("/myAccount")
	public Accounts getAccountDetails(@RequestBody Customer customer) {

		Accounts accounts = accountsRepository.findByCustomerId(customer.getCustomerId());
		if (accounts != null) {
			return accounts;
		} else {
			return null;
		}

	}
	
	@GetMapping("/account/properties")
	public String getPropertyDetails() throws JsonProcessingException {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		Properties properties = new Properties(accountsConfig.getMsg(), accountsConfig.getBuildVersion(),
				accountsConfig.getMailDetails(), accountsConfig.getActiveBranches());
		String jsonStr = ow.writeValueAsString(properties);
		return jsonStr;
	}
	
	@PostMapping("/myCustomerDetails")
	@CircuitBreaker(name = "detailsForCustomerSupportApp",fallbackMethod="myCustomerDetailsFallBack")
	@Retry(name = "retryForCustomerDetails", fallbackMethod = "myCustomerDetailsFallBack")
	public CustomerDetails myCustomerDetails(@RequestHeader("bankapp-correlation-id") String correlationid,@RequestBody Customer customer) {
		logger.info("myCustomerDetails() method started");
		Accounts accounts = accountsRepository.findByCustomerId(customer.getCustomerId());
		List<Loans> loans = loansFeignClient.getLoansDetails(correlationid,customer);
		List<Cards> cards = cardsFeignClient.getCardDetails(correlationid,customer);

		CustomerDetails customerDetails = new CustomerDetails();
		customerDetails.setAccounts(accounts);
		customerDetails.setLoans(loans);
		customerDetails.setCards(cards);
		logger.info("myCustomerDetails() method ended");
		return customerDetails;
	}
	
	private CustomerDetails myCustomerDetailsFallBack(@RequestHeader("bankapp-correlation-id") String correlationid,Customer customer, Throwable t) {
		Accounts accounts = accountsRepository.findByCustomerId(customer.getCustomerId());
		List<Loans> loans = loansFeignClient.getLoansDetails(correlationid,customer);
		CustomerDetails customerDetails = new CustomerDetails();
		customerDetails.setAccounts(accounts);
		customerDetails.setLoans(loans);
		return customerDetails;

	}
	
	@GetMapping("/sayHello")
	@RateLimiter(name = "sayHello", fallbackMethod = "sayHelloFallback")
	public String sayHello() {
		return "Hello, Welcome to BankApp";
	}

	private String sayHelloFallback(Throwable t) {
		return "Hi, Welcome to BankApp";
	}

}
```
### \loans\src\main\java\com\openup\loans\controller\LoansController.java

```java
/**
 * 
 */
package com.openup.loans.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.openup.loans.config.LoansServiceConfig;
import com.openup.loans.model.Customer;
import com.openup.loans.model.Loans;
import com.openup.loans.model.Properties;
import com.openup.loans.repository.LoansRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

/**
 * 
 *
 */

@RestController
public class LoansController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoansController.class);

	@Autowired
	private LoansRepository loansRepository;
	
	@Autowired
	LoansServiceConfig loansConfig;

	@PostMapping("/myLoans")
	public List<Loans> getLoansDetails(@RequestHeader("bankapp-correlation-id") String correlationid,@RequestBody Customer customer) {
		logger.info("getLoansDetails() method started");
		List<Loans> loans = loansRepository.findByCustomerIdOrderByStartDtDesc(customer.getCustomerId());
		logger.info("getLoansDetails() method ended");
		if (loans != null) {
			return loans;
		} else {
			return null;
		}

	}
	
	@GetMapping("/loans/properties")
	public String getPropertyDetails() throws JsonProcessingException {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		Properties properties = new Properties(loansConfig.getMsg(), loansConfig.getBuildVersion(),
				loansConfig.getMailDetails(), loansConfig.getActiveBranches());
		String jsonStr = ow.writeValueAsString(properties);
		return jsonStr;
	}

}
```
### \cards\src\main\java\com\openup\cards\controller\CardsController.java

```java
/**
 * 
 */
package com.openup.cards.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.openup.cards.config.CardsServiceConfig;
import com.openup.cards.model.Cards;
import com.openup.cards.model.Customer;
import com.openup.cards.model.Properties;
import com.openup.cards.repository.CardsRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

/**
 * 
 *
 */

@RestController
public class CardsController {
	
	private static final Logger logger = LoggerFactory.getLogger(CardsController.class);

	@Autowired
	private CardsRepository cardsRepository;
	
	@Autowired
	CardsServiceConfig cardsConfig;

	@PostMapping("/myCards")
	public List<Cards> getCardDetails(@RequestHeader("bankapp-correlation-id") String correlationid,@RequestBody Customer customer) {
		logger.info("getCardDetails() method started");
		List<Cards> cards = cardsRepository.findByCustomerId(customer.getCustomerId());
		logger.info("getCardDetails() method ended");
		if (cards != null) {
			return cards;
		} else {
			return null;
		}

	}
	
	@GetMapping("/cards/properties")
	public String getPropertyDetails() throws JsonProcessingException {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		Properties properties = new Properties(cardsConfig.getMsg(), cardsConfig.getBuildVersion(),
				cardsConfig.getMailDetails(), cardsConfig.getActiveBranches());
		String jsonStr = ow.writeValueAsString(properties);
		return jsonStr;
	}

}
```
- Start all the microservices in the order of **configserver, eurekaserver, accounts, loans, cards, gatewayserver**.
- Once all the microservices are started, access the URL http://127.0.0.1:8072/accounts/myCusomerDetails through Postman by passing the below request in JSON format. 
  You should be able to see the logger statements along with **App name, Trace ID, Span ID** like we discussed in the course.
  ```json
  {
    "customerId": 1
  }
  ```

### Implementing Distributed tracing & Log Aggregation inside microservices network using Spring Cloud Sleuth, Zipkin - PART 2

 - Now in order to use distributed tracing using **Zipkin**, run the docker command **'docker run -d -p 9411:9411 openzipkin/zipkin'**. This docker command will start the
   zipkin docker container using the provided docker image.
 - To validate if the zipkin server started successfully or not, visit the URL http://127.0.0.1:9411/zipkin inside your browser. You should be able to see the zipkin home
   page.
 - Stop all the microservices that are previously started in order to update them with zipkin related changes.
 - Open the **pom.xml** of all the microservices **accounts, loans, cards, configserver, eurekaserver, gatewayserver** and make sure to add the below required dependency of 
  **Zipkin** in all of them. 
  ```xml
   <dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-sleuth-zipkin</artifactId>
   </dependency>
  ```
- Open the **application.properties** of all the microservices **accounts, loans, cards, configserver, eurekaserver, gatewayserver** and make sure to add the below 
  properties/configurations in all of them.
  ```
  spring.sleuth.sampler.percentage=1
  spring.zipkin.baseUrl=http://127.0.0.1:9411/
  ```
- Start all the microservices in the order of **configserver, eurekaserver, accounts, loans, cards, gatewayserver**.
- Once all the microservices are started, access the URL http://127.0.0.1:8072/bankapp/accounts/myCusomerDetails through Postman by passing the below request in JSON format. 
  You should be able to see the tracing details inside zipkin console like we discussed in the course.
  ```json
  {
    "customerId": 1
  }
  ```
- Stop all the microservices that are previously started in order to update them with Rabbit MQ related changes.
- Now in order to push all the loggers into Rabbit MQ asynchronously, open the **pom.xml** of all the microservices **accounts, loans, cards, configserver, eurekaserver,           gatewayserver** and make sure to add the below required dependency of **Rabbit MQ** in all of them. 
  ```xml
   <dependency>	
	<groupId>org.springframework.amqp</groupId>
	<artifactId>spring-rabbit</artifactId>
   </dependency>
  ```
- Now in order to setup a Rabbit MQ server, run the docker command **'docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management'**. This docker command will start the Rabbit MQ related docker container using the provided docker image.
 - To validate if the Rabbit MQ server started successfully or not, visit the URL http://127.0.0.1:15672 inside your browser and login with username/password as **guest**
   like we discussed in the course. Please validate all the Connections, Channels, Queues etc. are empty.
 - Like we discussed in the course, create a new queue with the name **zipkin**.
 - Open the **application.properties** of all the microservices **accounts, loans, cards, configserver, eurekaserver, gatewayserver** and make sure to add the below 
  properties/configurations in all of them.
  ```
  spring.zipkin.sender.type=rabbit
  spring.zipkin.rabbitmq.queue=zipkin
  spring.rabbitmq.host=localhost
  spring.rabbitmq.port=5672
  spring.rabbitmq.username=guest
  spring.rabbitmq.password=guest
  ```
- Start all the microservices in the order of **configserver, eurekaserver, accounts, loans, cards, gatewayserver**.
- Once all the microservices are started, access the URL http://127.0.0.1:8072/accounts/myCusomerDetails through Postman by passing the below request in JSON format. 
  You should be able to see the tracing details inside Rabbit MQ console like we discussed in the course.
  ```json
  {
    "customerId": 1
  }
  ```
- Stop all the microservices that are running inside the Netbeans.
- Before generating the docker images for all the microservices, comment all the changes related to Rabbit MQ inside all the microservices like we discussed in the course.
- Generate the docker images for all the microservices and push them into Docker hub by following the similar steps we discussed in the previous sections.
- Now write docker-compose.yml files inside accounts/docker-compose folder for each profile with the following content,
### \accounts\docker-compose\default\docker-compose.yml
```yaml
version: "3.8"

services:

  zipkin:
    image: openzipkin/zipkin
    mem_limit: 700m
    ports:
      - "9411:9411"
    networks:
     - bankapp
  
  configserver:
    image: openup/configserver:latest
    mem_limit: 700m
    ports:
      - "8071:8071"
    networks:
     - bankapp
    depends_on:
      - zipkin
    environment:
      SPRING_PROFILES_ACTIVE: default
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
   
  eurekaserver:
    image: openup/eurekaserver:latest
    mem_limit: 700m
    ports:
      - "8070:8070"
    networks:
     - bankapp
    depends_on:
      - configserver
    deploy:
      restart_policy:
        condition: on-failure
        delay: 15s
        max_attempts: 10
        window: 120s
    environment:
      SPRING_PROFILES_ACTIVE: default
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
      
  accounts:
    image: openup/accounts:latest
    mem_limit: 700m
    ports:
      - "8080:8080"
    networks:
      - bankapp
    depends_on:
      - configserver
      - eurekaserver
    deploy:
      restart_policy:
        condition: on-failure
        delay: 30s
        max_attempts: 10
        window: 120s
    environment:
      SPRING_PROFILES_ACTIVE: default
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      EUREKA_CLIENT_SERVICEURL_DEFAULTZONE: http://eurekaserver:8070/eureka/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
  
  loans:
    image: openup/loans:latest
    mem_limit: 700m
    ports:
      - "8090:8090"
    networks:
      - bankapp
    depends_on:
      - configserver
      - eurekaserver
    deploy:
      restart_policy:
        condition: on-failure
        delay: 30s
        max_attempts: 10
        window: 120s
    environment:
      SPRING_PROFILES_ACTIVE: default
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      EUREKA_CLIENT_SERVICEURL_DEFAULTZONE: http://eurekaserver:8070/eureka/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
    
  cards:
    image: openup/cards:latest
    mem_limit: 700m
    ports:
      - "9000:9000"
    networks:
      - bankapp
    depends_on:
      - configserver
      - eurekaserver
    deploy:
      restart_policy:
        condition: on-failure
        delay: 30s
        max_attempts: 10
        window: 120s
    environment:
      SPRING_PROFILES_ACTIVE: default
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      EUREKA_CLIENT_SERVICEURL_DEFAULTZONE: http://eurekaserver:8070/eureka/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
   
  gatewayserver:
    image: openup/gatewayserver:latest
    mem_limit: 700m
    ports:
      - "8072:8072"
    networks:
      - bankapp
    depends_on:
      - configserver
      - eurekaserver
      - cards
      - loans
      - accounts
    deploy:
      restart_policy:
        condition: on-failure
        delay: 45s
        max_attempts: 10
        window: 180s
    environment:
      SPRING_PROFILES_ACTIVE: default
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      EUREKA_CLIENT_SERVICEURL_DEFAULTZONE: http://eurekaserver:8070/eureka/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
      
networks:
  bankapp:
```
### \accounts\docker-compose\dev\docker-compose.yml
```yaml
version: "3.8"

services:

  zipkin:
    image: openzipkin/zipkin
    mem_limit: 700m
    ports:
      - "9411:9411"
    networks:
     - bankapp
     
  configserver:
    image: openup/configserver:latest
    mem_limit: 700m
    ports:
      - "8071:8071"
    networks:
     - bankapp
    depends_on:
      - zipkin
    environment:
      SPRING_PROFILES_ACTIVE: dev
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
   
  eurekaserver:
    image: openup/eurekaserver:latest
    mem_limit: 700m
    ports:
      - "8070:8070"
    networks:
     - bankapp
    depends_on:
      - configserver
    deploy:
      restart_policy:
        condition: on-failure
        delay: 15s
        max_attempts: 10
        window: 120s
    environment:
      SPRING_PROFILES_ACTIVE: dev
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
      
  accounts:
    image: openup/accounts:latest
    mem_limit: 700m
    ports:
      - "8080:8080"
    networks:
      - bankapp
    depends_on:
      - configserver
      - eurekaserver
    deploy:
      restart_policy:
        condition: on-failure
        delay: 30s
        max_attempts: 10
        window: 120s
    environment:
      SPRING_PROFILES_ACTIVE: dev
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      EUREKA_CLIENT_SERVICEURL_DEFAULTZONE: http://eurekaserver:8070/eureka/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
  
  loans:
    image: openup/loans:latest
    mem_limit: 700m
    ports:
      - "8090:8090"
    networks:
      - bankapp
    depends_on:
      - configserver
      - eurekaserver
    deploy:
      restart_policy:
        condition: on-failure
        delay: 30s
        max_attempts: 10
        window: 120s
    environment:
      SPRING_PROFILES_ACTIVE: dev
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      EUREKA_CLIENT_SERVICEURL_DEFAULTZONE: http://eurekaserver:8070/eureka/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
    
  cards:
    image: openup/cards:latest
    mem_limit: 700m
    ports:
      - "9000:9000"
    networks:
      - bankapp
    depends_on:
      - configserver
      - eurekaserver
    deploy:
      restart_policy:
        condition: on-failure
        delay: 30s
        max_attempts: 10
        window: 120s
    environment:
      SPRING_PROFILES_ACTIVE: dev
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      EUREKA_CLIENT_SERVICEURL_DEFAULTZONE: http://eurekaserver:8070/eureka/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
  
  gatewayserver:
    image: openup/gatewayserver:latest
    mem_limit: 700m
    ports:
      - "8072:8072"
    networks:
      - bankapp
    depends_on:
      - configserver
      - eurekaserver
      - cards
      - loans
      - accounts
    deploy:
      restart_policy:
        condition: on-failure
        delay: 45s
        max_attempts: 10
        window: 180s
    environment:
      SPRING_PROFILES_ACTIVE: dev
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      EUREKA_CLIENT_SERVICEURL_DEFAULTZONE: http://eurekaserver:8070/eureka/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
    
networks:
  bankapp:
```
### \accounts\docker-compose\prod\docker-compose.yml
```yaml
version: "3.8"

services:

  zipkin:
    image: openzipkin/zipkin
    mem_limit: 700m
    ports:
      - "9411:9411"
    networks:
     - bankapp
     
  configserver:
    image: openup/configserver:latest
    mem_limit: 700m
    ports:
      - "8071:8071"
    networks:
     - bankapp
    depends_on:
      - zipkin
    environment:
      SPRING_PROFILES_ACTIVE: prod
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
   
  eurekaserver:
    image: openup/eurekaserver:latest
    mem_limit: 700m
    ports:
      - "8070:8070"
    networks:
     - bankapp
    depends_on:
      - configserver
    deploy:
      restart_policy:
        condition: on-failure
        delay: 15s
        max_attempts: 10
        window: 120s
    environment:
      SPRING_PROFILES_ACTIVE: prod
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
      
  accounts:
    image: openup/accounts:latest
    mem_limit: 700m
    ports:
      - "8080:8080"
    networks:
      - bankapp
    depends_on:
      - configserver
      - eurekaserver
    deploy:
      restart_policy:
        condition: on-failure
        delay: 30s
        max_attempts: 10
        window: 120s
    environment:
      SPRING_PROFILES_ACTIVE: prod
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      EUREKA_CLIENT_SERVICEURL_DEFAULTZONE: http://eurekaserver:8070/eureka/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
  
  loans:
    image: openup/loans:latest
    mem_limit: 700m
    ports:
      - "8090:8090"
    networks:
      - bankapp
    depends_on:
      - configserver
      - eurekaserver
    deploy:
      restart_policy:
        condition: on-failure
        delay: 30s
        max_attempts: 10
        window: 120s
    environment:
      SPRING_PROFILES_ACTIVE: prod
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      EUREKA_CLIENT_SERVICEURL_DEFAULTZONE: http://eurekaserver:8070/eureka/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
    
  cards:
    image: openup/cards:latest
    mem_limit: 700m
    ports:
      - "9000:9000"
    networks:
      - bankapp
    depends_on:
      - configserver
      - eurekaserver
    deploy:
      restart_policy:
        condition: on-failure
        delay: 30s
        max_attempts: 10
        window: 120s
    environment:
      SPRING_PROFILES_ACTIVE: prod
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      EUREKA_CLIENT_SERVICEURL_DEFAULTZONE: http://eurekaserver:8070/eureka/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
  
  gatewayserver:
    image: openup/gatewayserver:latest
    mem_limit: 700m
    ports:
      - "8072:8072"
    networks:
      - bankapp
    depends_on:
      - configserver
      - eurekaserver
      - cards
      - loans
      - accounts
    deploy:
      restart_policy:
        condition: on-failure
        delay: 45s
        max_attempts: 10
        window: 180s
    environment:
      SPRING_PROFILES_ACTIVE: prod
      SPRING_CONFIG_IMPORT: configserver:http://configserver:8071/
      EUREKA_CLIENT_SERVICEURL_DEFAULTZONE: http://eurekaserver:8070/eureka/
      SPRING_ZIPKIN_BASEURL: http://zipkin:9411/
      
networks:
  bankapp:
```
- Based on the active profile that you want start the microservices, open the command line tool where the docker-compose.yml is present and run the docker compose command **"docker-compose up"** to start all the microservices containers with a single command. All the running containers can be validated by running a docker command **"docker ps"**.
- To test the distributed tracing changes along with log aggregation, access the URL http://127.0.0.1:8072/accounts/myCusomerDetails through Postman by passing the below 
  request in JSON format. You should be able to see the tracing details inside zipkin console like we discussed in the course.
  ```json
  {
    "customerId": 1
  }
  ```
- Stop all the running containers by executing the docker compose command "docker-compose down" from the location where docker-compose.yml is present.

---
### HURRAY !!! Congratulations, you successfully implemented Distributed tracing & Log Aggregation inside microservices network using Spring Cloud Sleuth, Zipkin
---
